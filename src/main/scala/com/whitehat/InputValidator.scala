package com.whitehat

import java.security.InvalidParameterException

import scala.util.{Failure, Success, Try}

object InputValidator {

  def inputPresent(input: Iterator[String]): Try[String] = {
    if(input.hasNext){
      Success(input.next())
    }else{
      Failure(new NoSuchElementException("End of file reached"))
    }
  }

  def validate(input: Iterator[String]): Try[Move] = {
    for {
      move <- inputPresent(input)
      _ <- validateLength(move)
      fromX <- letterToInt(move(0))
      fromY <- numberToInt(move(1))
      toX <- letterToInt(move(2))
      toY <- numberToInt(move(3))
    } yield UndeterminedMove(Position(fromX, fromY), Position(toX, toY))
  }

  private def validateLength(move: String): Try[Boolean] = {
    move.length match {
      case 4 => Success(true)
      case _ => Failure(new InvalidParameterException("A move must be exactly 4 characters"))
    }
  }

  private def letterToInt(letter: Char): Try[Int] = intInRange(letter.toUpper - 65)

  private def numberToInt(letter: Char): Try[Int] = intInRange(letter - 49)

  private def intInRange(int: Int): Try[Int] = {
    (int >= 0 && int < 8) match {
      case true => Success(int)
      case false => Failure(new InvalidParameterException("Figure index is out of range"))
    }
  }
}
