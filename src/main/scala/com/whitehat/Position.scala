package com.whitehat

case class Position(x: Int, y: Int)

object Position {

  def north(from: Position, northX: Int): Position = {
    Position(from.x, from.y + northX)
  }

  def northWest(from: Position, northX: Int, westX: Int): Position = {
    Position(from.x - westX, from.y + northX)
  }

  def northEast(from: Position, northX: Int, eastX: Int): Position = {
    Position(from.x + eastX, from.y + northX)
  }

  def south(from: Position, southX: Int): Position = {
    Position(from.x, from.y - southX)
  }

  def west(from: Position, westX: Int): Position = {
    Position(from.x - westX, from.y)
  }

  def east(from: Position, eastX: Int): Position = {
    Position(from.x + eastX, from.y)
  }

  def southWest(from: Position, southX: Int, westX: Int): Position = {
    Position(from.x - westX, from.y - southX)
  }

  def southEast(from: Position, southX: Int, eastX: Int): Position = {
    Position(from.x + eastX, from.y - southX)
  }

  def filterInvalid(pos: List[Position]): List[Position] =
    pos.filter(p => p.x >= 0 && p.x < 8 && p.y >= 0 && p.y < 8)

  def isPositionValid(p: Position): Boolean = {
    p.x >= 0 && p.x < 8 && p.y >= 0 && p.y < 8
  }
}
