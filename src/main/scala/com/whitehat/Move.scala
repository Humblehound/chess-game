package com.whitehat

trait Move{
  def from: Position
  def to: Position
}

case class UndeterminedMove(from: Position, to: Position) extends Move

case class RepositionMove(from: Position, to: Position) extends Move

case class AttackMove(from: Position, to: Position, target: ChessPiece) extends Move
