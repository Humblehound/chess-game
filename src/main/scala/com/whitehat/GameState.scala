package com.whitehat

import com.whitehat.GameState.FiguresOnBoard
import com.whitehat.Player.{Black, Color, White}

case class GameState(currentPlayer: Color, board: FiguresOnBoard, playerInCheck: Option[Color] = None)

object GameState{
  type FiguresOnBoard = Map[Position, ChessPiece]

  def getOppositePlayer(gameState: GameState): Color ={
    gameState.currentPlayer match{
      case White => Black
      case Black => White
    }
  }

  def initial(): GameState ={
    GameState(
      White,
      NewGameConfig.getAllFigures
    )
  }
}