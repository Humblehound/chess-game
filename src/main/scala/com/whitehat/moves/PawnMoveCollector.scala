package com.whitehat.moves

import com.whitehat.Player.{Black, White}
import com.whitehat.Position._
import com.whitehat._

class PawnMoveCollector(override val fromPosition: Position,
                        override val piece: ChessPiece,
                        override val figures: Map[Position, ChessPiece]) extends CoreMoveCollector(fromPosition, piece, figures) {

  override def collectAllMoves(): Seq[Move] = {
    piece.color match {
      case White => Seq(oneNorth, twoNorth, attackNorthEast, attackNorthWest).filter(_.isDefined).map(_.get)
      case Black => Seq(oneSouth, twoSouth, attackSouthEast, attackSouthWest).filter(_.isDefined).map(_.get)
    }
  }

  private def oneNorth: Option[Move] = {
    val oneNorth = north(fromPosition, 1)
    if (canMove(oneNorth)) Some(RepositionMove(fromPosition, oneNorth)) else None
  }

  private def oneSouth: Option[Move] = {
    val oneSouth = south(fromPosition, 1)
    if (canMove(oneSouth)) Some(RepositionMove(fromPosition, oneSouth)) else None
  }

  private def twoSouth: Option[Move] = {
    val twoSouth = south(fromPosition, 2)
    if (isInitialMove && canMove(twoSouth) && notBlocked(twoSouth)) Some(RepositionMove(fromPosition, twoSouth)) else None
  }

  private def twoNorth: Option[Move] = {
    val twoNorth = north(fromPosition, 2)
    if (isInitialMove && canMove(twoNorth) && notBlocked(twoNorth)) Some(RepositionMove(fromPosition, twoNorth)) else None
  }

  private def attackSouthEast: Option[Move] = {
    getAttackMoveOpt(southEast(fromPosition, 1, 1))
  }

  private def attackSouthWest: Option[Move] = {
    getAttackMoveOpt(southWest(fromPosition, 1, 1))
  }

  private def attackNorthEast: Option[Move] = {
    getAttackMoveOpt(northEast(fromPosition, 1, 1))
  }

  private def attackNorthWest: Option[Move] = {
    getAttackMoveOpt(northWest(fromPosition, 1, 1))
  }

  private def getAttackMoveOpt(targetPosition: Position): Option[Move] = {
    val target = figures.get(targetPosition)
    if (canAttack(targetPosition, target)) {
      Some(AttackMove(fromPosition, targetPosition, target.get))
    } else {
      None
    }
  }

  private def notBlocked(position: Position): Boolean = {
    // hacky but will do
    val blockingOpt = Position(fromPosition.x, (fromPosition.y+position.y)/2)
    !figures.isDefinedAt(blockingOpt)
  }

  private def isInitialMove: Boolean = {
    piece.color match {
      case White => fromPosition.y == 1
      case Black => fromPosition.y == 6
    }
  }

  private def canMove(position: Position): Boolean = {
    isPositionValid(position) && figures.get(position).isEmpty
  }

  private def canAttack(position: Position, target: Option[ChessPiece]): Boolean = {
    isPositionValid(position) && target.isDefined && target.get.color != piece.color
  }
}
