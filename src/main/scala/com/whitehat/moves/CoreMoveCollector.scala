package com.whitehat.moves

import com.whitehat.Position.isPositionValid
import com.whitehat._

import scala.annotation.tailrec

abstract class CoreMoveCollector(protected val fromPosition: Position,
                                 protected val piece: ChessPiece,
                                 protected val figures: Map[Position, ChessPiece]) extends MoveCollector{

  protected def attackOrMoveTo(position: Position): Option[Move] = {
    val targetPosition = figures.get(position)
    (isPositionValid(position), targetPosition) match {
      case (true, Some(target)) => attackMoveIfOppositeColor(position, target)
      case (true, None) => Some(RepositionMove(fromPosition, position))
      case (false, _) => None
    }
  }

  private[moves] def collectAllMovesNorth: Seq[Move] = {
    goNorth(Position(fromPosition.x, fromPosition.y + 1), Seq())
  }

  private[moves] def collectAllMovesNorthWest: Seq[Move] = {
    goNorthWest(Position(fromPosition.x - 1, fromPosition.y + 1), Seq())
  }

  private[moves] def collectAllMovesNorthEast: Seq[Move] = {
    goNorthEast(Position(fromPosition.x + 1, fromPosition.y + 1), Seq())
  }

  private[moves] def collectAllMovesSouth: Seq[Move] = {
    goSouth(Position(fromPosition.x, fromPosition.y - 1), Seq())
  }

  private[moves] def collectAllMovesSouthWest: Seq[Move] = {
    goSouthWest(Position(fromPosition.x - 1, fromPosition.y - 1), Seq())
  }

  private[moves] def collectAllMovesSouthEast: Seq[Move] = {
    goSouthEast(Position(fromPosition.x + 1, fromPosition.y - 1), Seq())
  }

  private[moves] def collectAllMovesEast: Seq[Move] = {
    goEast(Position(fromPosition.x + 1, fromPosition.y), Seq())
  }

  private[moves] def collectAllMovesWest: Seq[Move] = {
    goWest(Position(fromPosition.x - 1, fromPosition.y), Seq())
  }

  @tailrec
  private def goNorth(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y > 7) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goNorth(Position(position.x, position.y + 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goNorthWest(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y > 7 || position.x < 0) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goNorthWest(Position(position.x - 1, position.y + 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goNorthEast(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y > 7 || position.x > 7) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goNorthEast(Position(position.x + 1, position.y + 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goSouth(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y < 0) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goSouth(Position(position.x, position.y - 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goSouthWest(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y < 0 || position.x < 0) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goSouthWest(Position(position.x - 1, position.y - 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goSouthEast(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.y < 0 || position.x > 7) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goSouthEast(Position(position.x + 1, position.y - 1), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goEast(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.x > 7) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goEast(Position(position.x + 1, position.y), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  @tailrec
  private def goWest(position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (position.x < 0) {
      return validPositions
    }

    val opt = figures.get(position)

    if (opt.isEmpty) {
      val repositionMove = RepositionMove(fromPosition, position)
      goWest(Position(position.x - 1, position.y), validPositions :+ repositionMove)
    } else attackMoveOrNone(opt, position, validPositions)
  }

  private def attackMoveIfOppositeColor(targetPosition: Position, target: ChessPiece): Option[Move] = {
    if (target.color != piece.color) Some(AttackMove(fromPosition, targetPosition, target)) else None
  }

  private def attackMoveOrNone(opt: Option[ChessPiece], position: Position, validPositions: Seq[Move]): Seq[Move] = {
    if (opt.get.color != piece.color) {
      return validPositions :+ AttackMove(fromPosition, position, opt.get)
    }
    validPositions
  }
}
