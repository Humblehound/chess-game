package com.whitehat.moves

import com.whitehat.{ChessPiece, Move, Position}

class RookMoveCollector(override val fromPosition: Position,
                        override val piece: ChessPiece,
                        override val figures: Map[Position, ChessPiece]) extends CoreMoveCollector(fromPosition, piece, figures) {
  override def collectAllMoves(): Seq[Move] = {
    collectAllMovesNorth ++ collectAllMovesWest ++ collectAllMovesEast ++ collectAllMovesSouth
  }
}
