package com.whitehat.moves

import com.whitehat.Move

trait MoveCollector {
  def collectAllMoves(): Seq[Move]
}
