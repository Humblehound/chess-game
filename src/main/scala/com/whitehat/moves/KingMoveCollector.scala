package com.whitehat.moves

import com.whitehat.{ChessPiece, Move, Position}
import com.whitehat.Position._

class KingMoveCollector(override val fromPosition: Position,
                        override val piece: ChessPiece,
                        override val figures: Map[Position, ChessPiece]) extends CoreMoveCollector(fromPosition, piece, figures) {
  override def collectAllMoves(): Seq[Move] =
    Seq(
      attackOrMoveTo(north(fromPosition, 1)),
      attackOrMoveTo(northWest(fromPosition, 1, 1)),
      attackOrMoveTo(northEast(fromPosition, 1, 1)),
      attackOrMoveTo(west(fromPosition, 1)),
      attackOrMoveTo(east(fromPosition, 1)),
      attackOrMoveTo(south(fromPosition, 1)),
      attackOrMoveTo(southWest(fromPosition, 1, 1)),
      attackOrMoveTo(southEast(fromPosition, 1, 1)),
    ).filter(_.isDefined).map(_.get)
}
