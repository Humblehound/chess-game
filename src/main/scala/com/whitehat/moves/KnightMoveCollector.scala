package com.whitehat.moves

import com.whitehat.Position._
import com.whitehat.{ChessPiece, Move, Position}

class KnightMoveCollector(override val fromPosition: Position,
                          override val piece: ChessPiece,
                          override val figures: Map[Position, ChessPiece]) extends CoreMoveCollector(fromPosition, piece, figures) {
  override def collectAllMoves(): Seq[Move] = {
    Seq(
      attackOrMoveTo(northWest(fromPosition, 2, 1)),
      attackOrMoveTo(northWest(fromPosition, 1, 2)),
      attackOrMoveTo(northEast(fromPosition, 2, 1)),
      attackOrMoveTo(northEast(fromPosition, 1, 2)),
      attackOrMoveTo(southWest(fromPosition, 2, 1)),
      attackOrMoveTo(southWest(fromPosition, 1, 2)),
      attackOrMoveTo(southEast(fromPosition, 2, 1)),
      attackOrMoveTo(southEast(fromPosition, 1, 2)),
    ).filter(_.isDefined).map(_.get)
  }
}
