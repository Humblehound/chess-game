package com.whitehat

import scala.io.Source
import scala.util.{Failure, Success, Try}

object ChessGame extends App {
  val game = new ChessGame()
  game.start()
}

class ChessGame() {

  val input: Iterator[String] = Source.fromResource("checkmate.txt").getLines

  def runGameRound(gameState: GameState): Try[GameState] = {
    for {
      move <- InputValidator.validate(input)
      figure <- MoveValidator.validate(move, gameState)
      newGameState <- Try(GameStateManager.executeMove(move, figure, gameState))
      _ = GameRenderer.draw(newGameState)
      _ <- GameStateManager.determineCheckMate(newGameState)
    } yield newGameState
  }

  def start(): Unit = {
    var currentGameState = GameState.initial()
    GameRenderer.draw(currentGameState)

    while (true) {
      runGameRound(currentGameState) match {
        case Success(newGameState) => currentGameState = newGameState
        case Failure(ex: GameOverException) => {
          println(s"Checkmate, player ${ex.playerThatWon} has won")
          System.exit(0)
        }
        case Failure(_: NoSuchElementException) => System.exit(0)
        case Failure(ex: Throwable) => println(ex.getMessage)
      }
    }
  }

}
