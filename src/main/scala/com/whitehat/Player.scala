package com.whitehat

object Player {
  sealed trait Color
  case object White extends Color
  case object Black extends Color
}
