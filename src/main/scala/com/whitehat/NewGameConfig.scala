package com.whitehat

import com.whitehat.Player.{Black, White}

object NewGameConfig {

  private val whitePawns = (0 to 7).map(x => Position(x, 1) -> Pawn(White)).toMap
  private val blackPawns = (0 to 7).map(x => Position(x, 6) -> Pawn(Black)).toMap
  private val rooks = Map(
    Position(0, 0) -> Rook(White),
    Position(7, 0) -> Rook(White),
    Position(0, 7) -> Rook(Black),
    Position(7, 7) -> Rook(Black),
  )
  private val knights = Map(
    Position(1, 0) -> Knight(White),
    Position(6, 0) -> Knight(White),
    Position(1, 7) -> Knight(Black),
    Position(6, 7) -> Knight(Black),
  )
  private val bishops = Map(
    Position(2, 0) -> Bishop(White),
    Position(5, 0) -> Bishop(White),
    Position(2, 7) -> Bishop(Black),
    Position(5, 7) -> Bishop(Black),
  )
  private val queens = Map(
    Position(3, 0) -> Queen(White),
    Position(3, 7) -> Queen(Black)
  )
  private val kings = Map(
    Position(4, 0) -> King(White),
    Position(4, 7) -> King(Black)
  )

  def getAllFigures: Map[Position, ChessPiece] = {
    whitePawns ++
      blackPawns ++
      rooks ++
      knights ++
      bishops ++
      queens ++
      kings
  }
}
