package com.whitehat

import com.whitehat.GameState.FiguresOnBoard
import com.whitehat.Player.Color

import scala.util.{Failure, Success, Try}

object MoveValidator {


  def checkIfStillInCheckAfterMove(figure: ChessPiece, move: Move, gameState: GameState): Try[Boolean] = {
    val stateAfterTheMove = GameStateManager.executeMove(move, figure, gameState)

    (gameState.playerInCheck, stateAfterTheMove.playerInCheck) match {
      case (Some(playerInCheckBefore), Some(playerInCheckAfter)) =>
        if (playerInCheckBefore == playerInCheckAfter) {
          Failure(new Exception("A player must make a move that removes the check"))
        } else Success(true)
      case _ => Success(true)
    }
  }

  def validate(move: Move, gameState: GameState): Try[ChessPiece] = {
    for {
      figure <- checkIfFigurePresent(move, gameState.board)
      _ <- checkIfValidPlayerMove(figure, gameState.currentPlayer)
      _ <- checkIfThereIsDistance(move)
      _ <- checkIfMoveAllowed(figure, move, gameState.board)
      _ <- checkIfStillInCheckAfterMove(figure, move, gameState)
    } yield figure
  }

  private def checkIfFigurePresent(move: Move, figurePositions: FiguresOnBoard): Try[ChessPiece] = {
    val figureOpt = figurePositions.get(move.from)
    if (figureOpt.isEmpty) {
      Failure(new Exception("Figure is missing"))
    } else {
      Success(figureOpt.get)
    }
  }

  private def checkIfMoveAllowed(figure: ChessPiece, move: Move, figurePositions: FiguresOnBoard): Try[Move] = {
    val moveCollector = figure.moveCollector(move.from, figurePositions)
    moveCollector.collectAllMoves().find(targetMove => targetMove.to == move.to) match {
      case Some(validMove) => Success(validMove)
      case _ => Failure(new Exception("Move is impossible"))
    }
  }

  private def checkIfValidPlayerMove(figure: ChessPiece, currentPlayer: Color): Try[Boolean] = {
    if (figure.color == currentPlayer) {
      Success(true)
    } else {
      Failure(new Exception("Invalid move: It is currently other player's turn"))
    }
  }

  private def checkIfThereIsDistance(move: Move): Try[Move] = {
    if (move.from == move.to) {
      Failure(new Exception("A figure must travel some distance"))
    } else {
      Success(move)
    }
  }
}
