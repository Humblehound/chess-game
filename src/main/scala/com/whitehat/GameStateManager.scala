package com.whitehat

import com.whitehat.GameState.FiguresOnBoard
import com.whitehat.Player.Color

import scala.util.{Failure, Success, Try}

object GameStateManager {

  private def updateBoard(gameState: GameState, figure: ChessPiece, move: Move): FiguresOnBoard = {
    val figureRemoved = gameState.board.-(move.from)
    figureRemoved + (move.to -> figure)
  }

  def executeMove(move: Move, figure: ChessPiece, gameState: GameState): GameState = {
    val newBoardState = updateBoard(gameState, figure, move)
    GameState(
      GameState.getOppositePlayer(gameState),
      newBoardState,
      determineCheck(newBoardState, gameState)
    )
  }

  def determineCheck(boardState: FiguresOnBoard, gameState: GameState): Option[Player.Color] = {
    val allAttackMovesNextRound: Seq[AttackMove] = boardState
      .flatMap { case (position, piece) => piece.moveCollector(position, boardState).collectAllMoves() }
      .collect {
        case attackMove: AttackMove => attackMove
      }.toSeq

    allAttackMovesNextRound.collect(move => move.target match {
      case _: King => Some(move.target.color)
      case _ => None
    }).find(_.isDefined).flatten
  }

  def determineCheckMate(gameState: GameState): Try[Boolean] = {
    gameState.playerInCheck match {
      case Some(currentPlayerInCheck) => isCheckMate(gameState, currentPlayerInCheck)
      case None => Success(true)
    }
  }

  private def isCheckMate(gameState: GameState, currentPlayerInCheck: Color): Try[Boolean] = {
    val movesOfCurrentPlayerInCheck: Map[ChessPiece, Move] = getMovesOfCurrentPlayerInCheck(gameState, currentPlayerInCheck)
    val movesThatInvalidateTheCheck = getMovesThatInvalidateTheCheck(movesOfCurrentPlayerInCheck, gameState, currentPlayerInCheck)
    if (movesThatInvalidateTheCheck.isEmpty) {
      Failure(GameOverException(GameState.getOppositePlayer(gameState)))
    } else {
      Success(true)
    }
  }

  private def getMovesOfCurrentPlayerInCheck(gameState: GameState, currentPlayerInCheck: Player.Color): Map[ChessPiece, Move] =
    gameState.board.filter(_._2.color == currentPlayerInCheck)
      .flatMap {
        case (position, piece) =>
          piece.moveCollector(position, gameState.board)
            .collectAllMoves()
            .map(move => (piece, move))
      }

  private def getMovesThatInvalidateTheCheck(moves: Map[ChessPiece, Move],
                                             gameState: GameState,
                                             currentPlayerInCheck: Player.Color) =
    moves.map {
      case (piece, move) => executeMove(move, piece, gameState)
    }.filterNot(_.playerInCheck.contains(currentPlayerInCheck))

}
