package com.whitehat

import com.whitehat.GameState.FiguresOnBoard

object GameRenderer {

  def draw(gameState: GameState): Unit = {
    println("")
    println("-----------------------------------")
    println("")
    for (y <- 7 to 0 by -1) {
      for (x <- 0 to 7) {
        print(getSymbolToDraw(gameState.board, x, y))
      }
      println("")
    }
    println("")

    if(gameState.playerInCheck.isDefined){
      println("")
      println(s"Player ${gameState.playerInCheck.get} is in check")
    }
  }

  def getSymbolToDraw(figures: FiguresOnBoard, x: Int, y: Int): Char = {
    val figureOpt = figures.get(Position(x, y))
    figureOpt match {
      case Some(figure) => figure.symbol()
      case None => '.'
    }
  }
}
