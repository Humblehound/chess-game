package com.whitehat

import com.whitehat.Player.Color

case class GameOverException(playerThatWon: Color) extends Throwable
