package com.whitehat

import com.whitehat.GameState.FiguresOnBoard
import com.whitehat.Player.{Black, Color, White}
import com.whitehat.moves._

sealed trait ChessPiece {
  def color: Color

  def symbol(): Char = {
    color match {
      case White => symbolCode.toUpper
      case Black => symbolCode.toLower
    }
  }

  def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector

  protected def symbolCode: Char
}

case class Pawn(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'P'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new PawnMoveCollector(from, this, figurePositions)
}

case class Rook(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'R'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new RookMoveCollector(from, this, figurePositions)
}

case class Knight(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'N'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new KnightMoveCollector(from, this, figurePositions)
}

case class Bishop(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'B'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new BishopMoveCollector(from, this, figurePositions)
}

case class Queen(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'Q'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new QueenMoveCollector(from, this, figurePositions)
}

case class King(color: Color) extends ChessPiece {
  override def symbolCode: Char = 'K'

  override def moveCollector(from: Position, figurePositions: FiguresOnBoard): MoveCollector =
    new KingMoveCollector(from, this, figurePositions)
}


