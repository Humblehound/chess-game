package com.whitehat

import com.whitehat.Player.{Black, White}
import org.scalatest.TryValues._
import org.scalatest.{Matchers, WordSpec}

class MoveValidatorSpec extends WordSpec with Matchers {
  "MoveValidator" should {
    "not allow a player to not act on a check" in {
      val king = King(Black)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 5), queen),
        (Position(3, 6), king)
      )
      val gameState = GameState(Black, board, Some(Black))
      val move = UndeterminedMove(Position(3, 6), Position(3, 7))
      val result = MoveValidator.checkIfStillInCheckAfterMove(king, move, gameState)

      result.isFailure shouldBe true
      result.failure.exception.getMessage shouldBe "A player must make a move that removes the check"
    }
    "remove the check if he removes the attacker" in {
      val king = King(Black)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 5), queen),
        (Position(3, 6), king)
      )
      val gameState = GameState(Black, board, Some(Black))
      val move = UndeterminedMove(Position(3, 6), Position(3, 5))
      val result = MoveValidator.checkIfStillInCheckAfterMove(king, move, gameState)

      result.isSuccess shouldBe true
    }
    "remove the check if he moves out of the attackers range" in {
      val king = King(Black)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 5), queen),
        (Position(3, 6), king)
      )
      val gameState = GameState(Black, board, Some(Black))
      val move = UndeterminedMove(Position(3, 6), Position(4, 7))
      val result = MoveValidator.checkIfStillInCheckAfterMove(king, move, gameState)

      result.isSuccess shouldBe true
    }
  }

}
