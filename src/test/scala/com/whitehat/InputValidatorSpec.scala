package com.whitehat

import java.security.InvalidParameterException

import org.scalatest.{Matchers, WordSpec}

import scala.util.{Failure, Success}

class InputValidatorSpec extends WordSpec with Matchers {
  "InputValidator" should {
    "parse valid move" in {
      val input = Iterator("e2c4")
      InputValidator.validate(input) should be (Success(UndeterminedMove(Position(4, 1), Position(2, 3))))
    }
    "reject invalid move 1" in {
      val input = Iterator("a0c1")
      InputValidator.validate(input).isFailure should be (true)
    }
    "reject invalid move 2" in {
      val input = Iterator("x1c1")
      InputValidator.validate(input).isFailure should be (true)
    }
    "reject invalid move 3" in {
      val input = Iterator("c3c3c")
      InputValidator.validate(input).isFailure should be (true)
    }
  }

}
