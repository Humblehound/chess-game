package com.whitehat.moves

import com.whitehat.Player.White
import com.whitehat.Position._
import com.whitehat._

class KingMoveCollectorSpec extends CollectorSpec {

  "A King" should {
    "be able to move in each direction" in {
      val startPosition = Position(3, 3)
      val piece = King(White)
      val emptyBoard = Map(startPosition -> piece)

      val kingMoveCollector = new KingMoveCollector(startPosition, piece, emptyBoard)
      sortMoves(kingMoveCollector.collectAllMoves()) should be(sortMoves(Seq(
        RepositionMove(startPosition, northWest(startPosition, 1, 1)),
        RepositionMove(startPosition, northEast(startPosition, 1, 1)),
        RepositionMove(startPosition, north(startPosition, 1)),
        RepositionMove(startPosition, west(startPosition, 1)),
        RepositionMove(startPosition, east(startPosition, 1)),
        RepositionMove(startPosition, south(startPosition, 1)),
        RepositionMove(startPosition, southEast(startPosition, 1, 1)),
        RepositionMove(startPosition, southWest(startPosition, 1, 1)),
      )))
    }
  }

}
