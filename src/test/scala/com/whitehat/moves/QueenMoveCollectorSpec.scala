package com.whitehat.moves

import com.whitehat.Player.White
import com.whitehat.{Position, Queen, RepositionMove}

class QueenMoveCollectorSpec extends CollectorSpec {

  "A Queen" should {
    "be able to move horizontally, vertically and diagonally" in {
      val startPosition = Position(3, 3)
      val piece = Queen(White)
      val emptyBoard = Map(startPosition -> piece)

      val queenMoveCollector = new QueenMoveCollector(startPosition, piece, emptyBoard)
      val expected = sortMoves(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        RepositionMove(startPosition, Position(3, 5)),
        RepositionMove(startPosition, Position(3, 6)),
        RepositionMove(startPosition, Position(3, 7)),
        RepositionMove(startPosition, Position(3, 2)),
        RepositionMove(startPosition, Position(3, 1)),
        RepositionMove(startPosition, Position(3, 0)),
        RepositionMove(startPosition, Position(2, 3)),
        RepositionMove(startPosition, Position(1, 3)),
        RepositionMove(startPosition, Position(0, 3)),
        RepositionMove(startPosition, Position(4, 3)),
        RepositionMove(startPosition, Position(5, 3)),
        RepositionMove(startPosition, Position(6, 3)),
        RepositionMove(startPosition, Position(7, 3)),
        RepositionMove(startPosition, Position(2, 4)),
        RepositionMove(startPosition, Position(1, 5)),
        RepositionMove(startPosition, Position(0, 6)),
        RepositionMove(startPosition, Position(4, 4)),
        RepositionMove(startPosition, Position(5, 5)),
        RepositionMove(startPosition, Position(6, 6)),
        RepositionMove(startPosition, Position(7, 7)),
        RepositionMove(startPosition, Position(2, 2)),
        RepositionMove(startPosition, Position(1, 1)),
        RepositionMove(startPosition, Position(0, 0)),
        RepositionMove(startPosition, Position(4, 2)),
        RepositionMove(startPosition, Position(5, 1)),
        RepositionMove(startPosition, Position(6, 0))
      ))
      sortMoves(queenMoveCollector.collectAllMoves()) should be(expected)
    }
  }
}
