package com.whitehat.moves

import com.whitehat.Player.{Black, White}
import com.whitehat._
import org.scalatest.{Matchers, WordSpec}

class CoreMoveCollectorSpec extends WordSpec with Matchers {

  "CoreMoveCollector" should {
    val startPosition = Position(3, 3)
    val piece = Rook(White)
    val emptyBoard = Map(startPosition -> piece)

    "collect all moves north if no obstacles found" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesNorth should be(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        RepositionMove(startPosition, Position(3, 5)),
        RepositionMove(startPosition, Position(3, 6)),
        RepositionMove(startPosition, Position(3, 7)),
      ))
    }

    "properly identify enemy figure the on the way north" in {
      val enemyPosition = Position(3, 5)
      val enemyPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesNorth should be(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        AttackMove(startPosition, Position(3, 5), enemyPiece),
      ))
    }

    "stop moving when same color figure met on the way north" in {
      val enemyPosition = Position(3, 5)
      val enemyPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesNorth should be(Seq(
        RepositionMove(startPosition, Position(3, 4))
      ))
    }

    "collect all moves south if no obstacles found" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesSouth should be(Seq(
        RepositionMove(startPosition, Position(3, 2)),
        RepositionMove(startPosition, Position(3, 1)),
        RepositionMove(startPosition, Position(3, 0)),
      ))
    }

    "properly identify enemy figure the on the way south" in {
      val enemyPosition = Position(3, 1)
      val enemyPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesSouth should be(Seq(
        RepositionMove(startPosition, Position(3, 2)),
        AttackMove(startPosition, Position(3, 1), enemyPiece),
      ))
    }

    "stop moving when same color figure met on the way south" in {
      val enemyPosition = Position(3, 1)
      val enemyPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesSouth should be(Seq(
        RepositionMove(startPosition, Position(3, 2))
      ))
    }

    "collect all moves west if no obstacles found" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesWest should be(Seq(
        RepositionMove(startPosition, Position(2, 3)),
        RepositionMove(startPosition, Position(1, 3)),
        RepositionMove(startPosition, Position(0, 3)),
      ))
    }

    "properly identify enemy figure the on the way west" in {
      val enemyPosition = Position(1, 3)
      val enemyPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesWest should be(Seq(
        RepositionMove(startPosition, Position(2, 3)),
        AttackMove(startPosition, Position(1, 3), enemyPiece),
      ))
    }

    "stop moving when same color figure met on the way west" in {
      val enemyPosition = Position(1, 3)
      val enemyPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesWest should be(Seq(
        RepositionMove(startPosition, Position(2, 3))
      ))
    }

    "collect all moves east if no obstacles found" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesEast should be(Seq(
        RepositionMove(startPosition, Position(4, 3)),
        RepositionMove(startPosition, Position(5, 3)),
        RepositionMove(startPosition, Position(6, 3)),
        RepositionMove(startPosition, Position(7, 3)),
      ))
    }

    "properly identify enemy figure the on the way east" in {
      val enemyPosition = Position(5, 3)
      val enemyPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesEast should be(Seq(
        RepositionMove(startPosition, Position(4, 3)),
        AttackMove(startPosition, Position(5, 3), enemyPiece),
      ))
    }

    "stop moving when same color figure met on the way east" in {
      val enemyPosition = Position(5, 3)
      val enemyPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (enemyPosition -> enemyPiece)
      val collectorStub = new CoreMoveCollector(startPosition, piece, boardWithEnemy) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesEast should be(Seq(
        RepositionMove(startPosition, Position(4, 3))
      ))
    }

    "properly move NW" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesNorthWest should be(Seq(
        RepositionMove(startPosition, Position(2, 4)),
        RepositionMove(startPosition, Position(1, 5)),
        RepositionMove(startPosition, Position(0, 6)),
      ))
    }

    "properly move NE" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesNorthEast should be(Seq(
        RepositionMove(startPosition, Position(4, 4)),
        RepositionMove(startPosition, Position(5, 5)),
        RepositionMove(startPosition, Position(6, 6)),
        RepositionMove(startPosition, Position(7, 7)),
      ))
    }

    "properly move SW" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesSouthWest should be(Seq(
        RepositionMove(startPosition, Position(2, 2)),
        RepositionMove(startPosition, Position(1, 1)),
        RepositionMove(startPosition, Position(0, 0))
      ))
    }

    "properly move SE" in {
      val collectorStub = new CoreMoveCollector(startPosition, piece, emptyBoard) {
        override def collectAllMoves(): Seq[Move] = ???
      }
      collectorStub.collectAllMovesSouthEast should be(Seq(
        RepositionMove(startPosition, Position(4, 2)),
        RepositionMove(startPosition, Position(5, 1)),
        RepositionMove(startPosition, Position(6, 0))
      ))
    }
  }

}
