package com.whitehat.moves

import com.whitehat.Move
import org.scalatest.{Matchers, WordSpec}

trait CollectorSpec extends WordSpec with Matchers{

  def sortMoves(moves: Seq[Move]) = moves.sortBy(x => (x.to.x, x.to.y))
}
