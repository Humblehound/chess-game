package com.whitehat.moves

import com.whitehat.Player.White
import com.whitehat.{Bishop, Position, RepositionMove, Rook}

class BishopMoveCollectorSpec extends CollectorSpec {
  "A Bishop" should {
    "be able to move diagonally" in {
      val startPosition = Position(3, 3)
      val piece = Bishop(White)
      val emptyBoard = Map(startPosition -> piece)

      val bishopMoveCollector = new BishopMoveCollector(startPosition, piece, emptyBoard)
      val expected = sortMoves(Seq(
        RepositionMove(startPosition, Position(2, 4)),
        RepositionMove(startPosition, Position(1, 5)),
        RepositionMove(startPosition, Position(0, 6)),
        RepositionMove(startPosition, Position(4, 4)),
        RepositionMove(startPosition, Position(5, 5)),
        RepositionMove(startPosition, Position(6, 6)),
        RepositionMove(startPosition, Position(7, 7)),
        RepositionMove(startPosition, Position(2, 2)),
        RepositionMove(startPosition, Position(1, 1)),
        RepositionMove(startPosition, Position(0, 0)),
        RepositionMove(startPosition, Position(4, 2)),
        RepositionMove(startPosition, Position(5, 1)),
        RepositionMove(startPosition, Position(6, 0))
      ))
      sortMoves(bishopMoveCollector.collectAllMoves()) should be(expected)
    }
  }
}
