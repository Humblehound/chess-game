package com.whitehat.moves

import com.whitehat.Player.{Black, White}
import com.whitehat.Position._
import com.whitehat._

class KnightMoveCollectorSpec extends CollectorSpec {

  "A Knight" should {
    "be able to jump in 8 directions" in {
      val startPosition = Position(3, 3)
      val piece = Knight(White)
      val emptyBoard = Map(startPosition -> piece)

      val knightMoveCollector = new KnightMoveCollector(startPosition, piece, emptyBoard)
      sortMoves(knightMoveCollector.collectAllMoves()) should be(sortMoves(Seq(
        RepositionMove(startPosition, northWest(startPosition, 1, 2)),
        RepositionMove(startPosition, northWest(startPosition, 2, 1)),
        RepositionMove(startPosition, northEast(startPosition, 1, 2)),
        RepositionMove(startPosition, northEast(startPosition, 2, 1)),
        RepositionMove(startPosition, southEast(startPosition, 1, 2)),
        RepositionMove(startPosition, southEast(startPosition, 2, 1)),
        RepositionMove(startPosition, southWest(startPosition, 1, 2)),
        RepositionMove(startPosition, southWest(startPosition, 2, 1)),
      )))
    }

    "not be able to jump out of the board" in {
      val startPosition = Position(0, 0)
      val piece = Knight(White)
      val emptyBoard = Map(startPosition -> piece)
      val knightMoveCollector = new KnightMoveCollector(startPosition, piece, emptyBoard)
      sortMoves(knightMoveCollector.collectAllMoves()) should be(sortMoves(Seq(
        RepositionMove(startPosition, northEast(startPosition, 1, 2)),
        RepositionMove(startPosition, northEast(startPosition, 2, 1))
      )))
    }

    "be able to attack opposing color" in {
      val startPosition = Position(0, 0)
      val piece = Knight(White)

      val targetPosition = northEast(startPosition, 1, 2)
      val targetPiece = Rook(Black)

      val emptyBoard = Map(startPosition -> piece, targetPosition -> targetPiece)

      val knightMoveCollector = new KnightMoveCollector(startPosition, piece, emptyBoard)
      sortMoves(knightMoveCollector.collectAllMoves()) should be(sortMoves(Seq(
        AttackMove(startPosition, northEast(startPosition, 1, 2), targetPiece),
        RepositionMove(startPosition, northEast(startPosition, 2, 1))
      )))
    }

    "not be able to attack same color" in {
      val startPosition = Position(0, 0)
      val piece = Knight(White)

      val targetPosition = northEast(startPosition, 1, 2)
      val targetPiece = Rook(White)

      val emptyBoard = Map(startPosition -> piece, targetPosition -> targetPiece)

      val knightMoveCollector = new KnightMoveCollector(startPosition, piece, emptyBoard)
      sortMoves(knightMoveCollector.collectAllMoves()) should be(sortMoves(Seq(
        RepositionMove(startPosition, northEast(startPosition, 2, 1))
      )))
    }
  }

}
