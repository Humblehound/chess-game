package com.whitehat.moves

import com.whitehat.Player.White
import com.whitehat.{Position, RepositionMove, Rook}

class RookMoveCollectorSpec extends CollectorSpec {

  "A Rook" should {
    "be able to move horizontally and vertically" in {
      val startPosition = Position(3, 3)
      val piece = Rook(White)
      val emptyBoard = Map(startPosition -> piece)

      val rookMoveCollector = new RookMoveCollector(startPosition, piece, emptyBoard)
      val expected = sortMoves(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        RepositionMove(startPosition, Position(3, 5)),
        RepositionMove(startPosition, Position(3, 6)),
        RepositionMove(startPosition, Position(3, 7)),
        RepositionMove(startPosition, Position(3, 2)),
        RepositionMove(startPosition, Position(3, 1)),
        RepositionMove(startPosition, Position(3, 0)),
        RepositionMove(startPosition, Position(2, 3)),
        RepositionMove(startPosition, Position(1, 3)),
        RepositionMove(startPosition, Position(0, 3)),
        RepositionMove(startPosition, Position(4, 3)),
        RepositionMove(startPosition, Position(5, 3)),
        RepositionMove(startPosition, Position(6, 3)),
        RepositionMove(startPosition, Position(7, 3)),
      ))
      sortMoves(rookMoveCollector.collectAllMoves()) should be(expected)
    }
  }

}
