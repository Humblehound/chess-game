package com.whitehat.moves

import com.whitehat.Player.{Black, White}
import com.whitehat._
import com.whitehat.Position._

class PawnMoveCollectorSpec extends CollectorSpec {

  "A white Pawn" should {
    val startPosition = Position(3, 3)
    val piece = Pawn(White)
    val emptyBoard = Map(startPosition -> piece)

    "should be able to move one north" in {
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, emptyBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 4))
      ))
    }

    "should be able to move two north if in start position" in {
      val startPosition = Position(3, 1)
      val piece = Pawn(White)
      val emptyBoard = Map(startPosition -> piece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, emptyBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 2)),
        RepositionMove(startPosition, Position(3, 3))
      ))
    }

    "should not be able to move two north if blocked" in {
      val startPosition = Position(3, 1)
      val piece = Pawn(White)

      val blockingPosition = Position(3, 2)
      val blockingPiece = Pawn(Black)

      val blockedBoard = Map(startPosition -> piece, blockingPosition -> blockingPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, blockedBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(
      ))
    }

    "should be able to attack NE" in {
      val targetPosition = Position(4, 4)
      val targetPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        AttackMove(startPosition, targetPosition, targetPiece)
      ))
    }

    "should be able to attack NW" in {
      val targetPosition = Position(2, 4)
      val targetPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 4)),
        AttackMove(startPosition, targetPosition, targetPiece)
      ))
    }

    "should not be able to attack the same color" in {
      val targetPosition = Position(4, 4)
      val targetPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 4))
      ))
    }
  }

  "A black Pawn" should {
    val startPosition = Position(3, 3)
    val piece = Pawn(Black)
    val emptyBoard = Map(startPosition -> piece)

    "should be able to move one south" in {
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, emptyBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(RepositionMove(startPosition, Position(3, 2))))
    }

    "should be able to move two south if in start position" in {
      val startPosition = Position(3, 6)
      val emptyBoard = Map(startPosition -> piece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, emptyBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 5)),
        RepositionMove(startPosition, Position(3, 4))
      ))
    }

    "should not be able to move two south if blocked" in {
      val startPosition = Position(3, 6)
      val piece = Pawn(Black)

      val blockingPosition = Position(3, 5)
      val blockingPiece = Pawn(White)

      val blockedBoard = Map(startPosition -> piece, blockingPosition -> blockingPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, blockedBoard)
      pawnMoveCollector.collectAllMoves() should be(Seq(
      ))
    }

    "should be able to attack SE" in {
      val targetPosition = southEast(startPosition, 1, 1)
      val targetPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 2)),
        AttackMove(startPosition, targetPosition, targetPiece)
      ))
    }

    "should be able to attack SW" in {
      val targetPosition = southWest(startPosition, 1, 1)
      val targetPiece = Rook(White)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 2)),
        AttackMove(startPosition, targetPosition, targetPiece)
      ))
    }

    "should not be able to attack the same color" in {
      val targetPosition = Position(4, 4)
      val targetPiece = Rook(Black)
      val boardWithEnemy = emptyBoard + (targetPosition -> targetPiece)
      val pawnMoveCollector = new PawnMoveCollector(startPosition, piece, boardWithEnemy)
      pawnMoveCollector.collectAllMoves() should be(Seq(
        RepositionMove(startPosition, Position(3, 2))
      ))
    }
  }
}
