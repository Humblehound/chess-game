package com.whitehat

import com.whitehat.Player.{Black, White}
import org.scalatest.{Matchers, WordSpec}
import org.scalatest.TryValues._


class GameStateManagerSpec extends WordSpec with Matchers {

  "GameStateManager" should {
    "determine check mate successfully" in {
      val king = King(Black)
      val rook = Rook(White)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 7), queen),
        (Position(3, 6), king),
        (Position(3, 5), rook)
      )
      val result = GameStateManager.determineCheckMate(GameState(Black, board, Some(Black)))

      result.isFailure should be(true)
      result.failure.exception shouldBe GameOverException(White)
    }
    "not determine check mate if counterattack possible" in {
      val king = King(Black)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 7), queen),
        (Position(3, 6), king),
      )
      val result = GameStateManager.determineCheckMate(GameState(Black, board, Some(Black)))

      result.isFailure should be(false)
    }
    "not determine check mate if escape possible" in {
      val king = King(Black)
      val queen = Queen(White)
      val board = Map(
        (Position(3, 6), queen),
        (Position(3, 5), king),
      )
      val result = GameStateManager.determineCheckMate(GameState(Black, board, Some(Black)))

      result.isFailure should be(false)
    }
  }
}
